import { Flight } from '../components/form/Flight';
export class Mock {
  public static mFlight: Flight[] = [
    {
      fullName: 'pop',
      from: 'Bangkok',
      to: 'Chiang Mai',
      type: 'One way',
      adults: 0,
      children: 2,
      infants: 1,
      departure: new Date(2565, 2, 10),
      arrival: new Date(2565, 2, 20),
    },
  ];
}
