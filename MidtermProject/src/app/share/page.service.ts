import { Injectable } from '@angular/core';
import { Flight } from '../components/form/Flight';
import { Mock } from './mock';
@Injectable({
  providedIn: 'root',
})
export class PageService {
  flights: Flight[] = [];
  constructor() {
    this.flights = Mock.mFlight;
  }
  getFlight(): Flight[] {
    return this.flights;
  }
  addFlight(f:Flight) {
    this.flights.push(f);
  }
}
