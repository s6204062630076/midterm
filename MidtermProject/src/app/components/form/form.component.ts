import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PageService } from 'src/app/share/page.service';
import { Flight } from './Flight';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  form:FormGroup;
  listFrom:string [] = ['Bangkok','Amnat Charoen','Ang Thong','Nonthaburi','Nakhon Sawan'];
  listTo:string [] = ['Chiang Mai','Chiang Rai','Lampang','Lamphun','Mae Hong Son'];
  flights!:Flight[];
  constructor(private fb:FormBuilder,private pageService:PageService) {
    this.form = this.fb.group({
      fullName:['',Validators.required],
      from:['',Validators.required],
      to:['',Validators.required],
      type:['',Validators.required],
      adults:[,Validators.required],
      departure:[,Validators.required],
      children:[,Validators.required],
      infants:[,Validators.required],
      arrival:[,Validators.required],
    })
  }

  ngOnInit(): void {
    this.getFlight();
  }
  onSubmit(f:Flight):void{
    this.pageService.addFlight(f);
    this.form.reset();
  }
  getFlight(){
    this.flights = this.pageService.getFlight();
  }
}
